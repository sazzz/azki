#!/usr/bin/env bash

CONTAINER_NAME="$1"
PROJECT_NAME="app"

# lets find the first container
FIRST_NUM=`docker ps | awk '{print $NF}' | grep app_$CONTAINER_NAME | awk -F  "_" '{print $NF}' | sort | head -1`
NUM_OF_CONTAINERS=1
MAX_NUM_OF_CONTAINERS=2

# if empty, first run
if [ -z "$FIRST_NUM" ]; then
  docker-compose -f docker-compose.yml -p $PROJECT_NAME up --build -d
else
  # pull new images
  docker-compose -f docker-compose.yml -p $PROJECT_NAME pull

  # scale & build new container
  docker-compose -f docker-compose.yml -p $PROJECT_NAME up \
    -d \
    --build \
    --no-deps \
    --scale $CONTAINER_NAME=$MAX_NUM_OF_CONTAINERS \
    $CONTAINER_NAME

  # waiting for new containers
  sleep 90

  # removing old containers
  for ((i=$FIRST_NUM;i<$NUM_OF_CONTAINERS+$FIRST_NUM;i++))
  do
    docker stop "${PROJECT_NAME}_${CONTAINER_NAME}_$i"
    docker rm "${PROJECT_NAME}_${CONTAINER_NAME}_$i"
  done

  docker-compose -f docker-compose.yml -p $PROJECT_NAME up \
    -d \
    --no-deps \
    --scale $CONTAINER_NAME=$NUM_OF_CONTAINERS \
    $CONTAINER_NAME
fi
